//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>
int input()
{
int x;
printf("enter a number\n");
scanf("%d",&x);
return x;
}

int sum(int a,int b)
{
int sum;
sum=a+b;
return sum;
}

int output(int x,int y,int z)
{
printf("sum of %d and %d is %d\n",x,y,z);
}

int main()
{
int x,y,z;
x=input();
y=input();
z=sum(x,y);
output(x,y,z);
return 0;
}
